﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.Common;
using System.Configuration;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace DataBase
{
    class DataBaseSupport
    {
      // obiekt przechowujący tabelę produktów
      DataSet dataBase = new DataSet();

      public SqlCeDataAdapter dataAdapter;
      ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["Produkty"];

      public void OpenDataBase()
      {
          DataTable tmp = new DataTable();

          using (SqlCeConnection connection = new SqlCeConnection(connectionString.ConnectionString))
          {
              try
              {
                  connection.Open();
                  dataAdapter = new SqlCeDataAdapter("Select * from Produkty order by Nazwa", connection);

                  using (SqlCeCommandBuilder cmd = new SqlCeCommandBuilder(dataAdapter))
                  {
                      dataAdapter.Fill(tmp);


                      if (dataBase.Tables.Count == 0)
                      {
                          dataBase.Tables.Add(tmp);
                      }
                      else
                      {
                          dataBase.Tables.Clear();
                          dataBase.Tables.Add(tmp);
                      }
                      SetColumnsName();
                  }
              }
              catch (Exception e)
              {
                  System.Windows.Forms.MessageBox.Show(e.Message, "Niestety nie udało się wykonać zapytania");
              }
           
          }
      }

      public bool UpdateDataBase()
      {
          bool isGood = true;

          using (SqlCeConnection connection = new SqlCeConnection(connectionString.ConnectionString))
          {
              try
              {
                  connection.Open();
                  dataAdapter = new SqlCeDataAdapter("Select * from produkty", connection);

                  using (SqlCeCommandBuilder cmd = new SqlCeCommandBuilder(dataAdapter))
                  {
                      dataAdapter.Update(dataBase,"Produkty");
                  }
              }
              catch (Exception e)
              {
                  MessageBox.Show(e.Message, "Niestety nie udało się wykonać zapytania");
                  isGood = false;
              }
          }

          return isGood;
      }

      public bool DeleteFromTable(int deletedProductIndex)
      {
          bool isGood = false;


          using (SqlCeConnection connection = new SqlCeConnection(connectionString.ConnectionString))
          {
              try
              {
                  connection.Open();

                  string querry = "Delete form Produkty where id ='" + deletedProductIndex.ToString() +"';";

                  using (SqlCeCommand cmd = new SqlCeCommand(querry, connection))
                  {
                      cmd.CommandType = CommandType.Text;
                      cmd.ExecuteNonQuery();
                      isGood = true;                     
                  }
              }
              catch (Exception e)
              {
                  MessageBox.Show("Niestety nie udało się usunąć produktu", e.Message);
              }

          }
          return isGood;
      }

      public bool CheckConnectionToDataBase()
        {
            bool isOk = false;

            using (SqlCeConnection connection = new SqlCeConnection(connectionString.ConnectionString))
            {
                try
                {
                    connection.Open();
                    isOk = true;
                }
                catch 
                {
                }
            }

            return isOk;
        }

        private void SetColumnsName()
        {
            dataBase.Tables[0].TableName = "Produkty";
        }

      public DataTable GetProductsTable
      {
          get { return dataBase.Tables["Produkty"]; }
      }

      public DataRow[] GetRecord(string name)
      {
          DataRow[] tmp = dataBase.Tables["Produkty"].Select("Nazwa ='" + name + "'");

          return tmp;
      }
    }
}
