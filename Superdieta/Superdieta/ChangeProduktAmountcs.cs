﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Superdieta
{
    public partial class ChangeProduktAmountcs : Form
    {
        string amount = "";

        public ChangeProduktAmountcs(string amount)
        {          
            InitializeComponent();
            textBox1.Text = amount;
            this.amount = amount;
        }

        private bool CheckAmountCorrectness()
        {
            if (ValidationClass.IsIntegerNumber(textBox1.Text) != true && textBox1.Text != "")
            {
                MessageBox.Show("Musisz podać ilość produktu jako liczbę!", "Błąd");
                return false;
            }
            else
            {
                this.amount = textBox1.Text;
                return true;
            }
        }


        public string ReturnAmount()
        {
            return this.amount;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            if (CheckAmountCorrectness() == true)
            {
                this.Close();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
