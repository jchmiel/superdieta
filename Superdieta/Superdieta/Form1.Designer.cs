﻿namespace Superdieta
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.productsNameCmbBox = new System.Windows.Forms.ComboBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.programToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zakończToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.produktyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dodajProduktToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.edytujProduktToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuńProduktToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chosenProduktsDataGridView = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fatTxtBox = new System.Windows.Forms.TextBox();
            this.proteinTxtBox = new System.Windows.Forms.TextBox();
            this.carboTxtBox = new System.Windows.Forms.TextBox();
            this.kcalTxtBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.productAmount = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.addProductButton = new System.Windows.Forms.Button();
            this.removeProductButton = new System.Windows.Forms.Button();
            this.clearProductsDataGridView = new System.Windows.Forms.Button();
            this.sumProductValueDataGridView = new System.Windows.Forms.DataGridView();
            this.chosenProduktCntxt = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chosenProduktsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sumProductValueDataGridView)).BeginInit();
            this.chosenProduktCntxt.SuspendLayout();
            this.SuspendLayout();
            // 
            // productsNameCmbBox
            // 
            this.productsNameCmbBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.productsNameCmbBox.FormattingEnabled = true;
            this.productsNameCmbBox.Location = new System.Drawing.Point(15, 27);
            this.productsNameCmbBox.Name = "productsNameCmbBox";
            this.productsNameCmbBox.Size = new System.Drawing.Size(198, 21);
            this.productsNameCmbBox.TabIndex = 0;
            this.productsNameCmbBox.SelectedIndexChanged += new System.EventHandler(this.productsNameCmbBox_SelectedIndexChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programToolStripMenuItem,
            this.produktyToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(786, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // programToolStripMenuItem
            // 
            this.programToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zakończToolStripMenuItem});
            this.programToolStripMenuItem.Name = "programToolStripMenuItem";
            this.programToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.programToolStripMenuItem.Text = "Program";
            // 
            // zakończToolStripMenuItem
            // 
            this.zakończToolStripMenuItem.Name = "zakończToolStripMenuItem";
            this.zakończToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.zakończToolStripMenuItem.Text = "Zakończ";
            this.zakończToolStripMenuItem.Click += new System.EventHandler(this.zakończToolStripMenuItem_Click);
            // 
            // produktyToolStripMenuItem
            // 
            this.produktyToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dodajProduktToolStripMenuItem,
            this.edytujProduktToolStripMenuItem,
            this.usuńProduktToolStripMenuItem});
            this.produktyToolStripMenuItem.Name = "produktyToolStripMenuItem";
            this.produktyToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.produktyToolStripMenuItem.Text = "Produkty";
            // 
            // dodajProduktToolStripMenuItem
            // 
            this.dodajProduktToolStripMenuItem.Name = "dodajProduktToolStripMenuItem";
            this.dodajProduktToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.dodajProduktToolStripMenuItem.Text = "Dodaj Produkt";
            this.dodajProduktToolStripMenuItem.Click += new System.EventHandler(this.dodajProduktToolStripMenuItem_Click);
            // 
            // edytujProduktToolStripMenuItem
            // 
            this.edytujProduktToolStripMenuItem.Name = "edytujProduktToolStripMenuItem";
            this.edytujProduktToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.edytujProduktToolStripMenuItem.Text = "Edytuj Produkt";
            this.edytujProduktToolStripMenuItem.Click += new System.EventHandler(this.edytujProduktToolStripMenuItem_Click);
            // 
            // usuńProduktToolStripMenuItem
            // 
            this.usuńProduktToolStripMenuItem.Name = "usuńProduktToolStripMenuItem";
            this.usuńProduktToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.usuńProduktToolStripMenuItem.Text = "Usuń Produkt";
            this.usuńProduktToolStripMenuItem.Click += new System.EventHandler(this.usuńProduktToolStripMenuItem_Click);
            // 
            // chosenProduktsDataGridView
            // 
            this.chosenProduktsDataGridView.AllowUserToAddRows = false;
            this.chosenProduktsDataGridView.AllowUserToDeleteRows = false;
            this.chosenProduktsDataGridView.AllowUserToResizeRows = false;
            this.chosenProduktsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chosenProduktsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.chosenProduktsDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.chosenProduktsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.chosenProduktsDataGridView.Location = new System.Drawing.Point(15, 153);
            this.chosenProduktsDataGridView.Name = "chosenProduktsDataGridView";
            this.chosenProduktsDataGridView.RowHeadersVisible = false;
            this.chosenProduktsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.chosenProduktsDataGridView.Size = new System.Drawing.Size(759, 342);
            this.chosenProduktsDataGridView.TabIndex = 2;
            this.chosenProduktsDataGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.chosenProduktsDataGridView_CellMouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Białko [ g ]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(111, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Tłuszcze [ g ]";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(214, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Węglowodany [ g ]";
            // 
            // fatTxtBox
            // 
            this.fatTxtBox.Location = new System.Drawing.Point(114, 87);
            this.fatTxtBox.Name = "fatTxtBox";
            this.fatTxtBox.Size = new System.Drawing.Size(93, 20);
            this.fatTxtBox.TabIndex = 8;
            // 
            // proteinTxtBox
            // 
            this.proteinTxtBox.Location = new System.Drawing.Point(15, 87);
            this.proteinTxtBox.Name = "proteinTxtBox";
            this.proteinTxtBox.Size = new System.Drawing.Size(93, 20);
            this.proteinTxtBox.TabIndex = 9;
            // 
            // carboTxtBox
            // 
            this.carboTxtBox.Location = new System.Drawing.Point(213, 87);
            this.carboTxtBox.Name = "carboTxtBox";
            this.carboTxtBox.Size = new System.Drawing.Size(93, 20);
            this.carboTxtBox.TabIndex = 10;
            // 
            // kcalTxtBox
            // 
            this.kcalTxtBox.Location = new System.Drawing.Point(315, 87);
            this.kcalTxtBox.Name = "kcalTxtBox";
            this.kcalTxtBox.Size = new System.Drawing.Size(93, 20);
            this.kcalTxtBox.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(316, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Kalorie [ kcal ]";
            // 
            // productAmount
            // 
            this.productAmount.Location = new System.Drawing.Point(321, 27);
            this.productAmount.Name = "productAmount";
            this.productAmount.Size = new System.Drawing.Size(93, 20);
            this.productAmount.TabIndex = 14;
            this.productAmount.KeyUp += new System.Windows.Forms.KeyEventHandler(this.productAmount_KeyUp);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(220, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Ilość produktu [ g ]";
            // 
            // addProductButton
            // 
            this.addProductButton.Location = new System.Drawing.Point(432, 27);
            this.addProductButton.Name = "addProductButton";
            this.addProductButton.Size = new System.Drawing.Size(75, 23);
            this.addProductButton.TabIndex = 16;
            this.addProductButton.Text = "Dodaj";
            this.addProductButton.UseVisualStyleBackColor = true;
            this.addProductButton.Click += new System.EventHandler(this.addProductButton_Click);
            // 
            // removeProductButton
            // 
            this.removeProductButton.Location = new System.Drawing.Point(432, 56);
            this.removeProductButton.Name = "removeProductButton";
            this.removeProductButton.Size = new System.Drawing.Size(75, 23);
            this.removeProductButton.TabIndex = 17;
            this.removeProductButton.Text = "Usuń";
            this.removeProductButton.UseVisualStyleBackColor = true;
            this.removeProductButton.Click += new System.EventHandler(this.removeProductButton_Click);
            // 
            // clearProductsDataGridView
            // 
            this.clearProductsDataGridView.Location = new System.Drawing.Point(432, 85);
            this.clearProductsDataGridView.Name = "clearProductsDataGridView";
            this.clearProductsDataGridView.Size = new System.Drawing.Size(75, 23);
            this.clearProductsDataGridView.TabIndex = 18;
            this.clearProductsDataGridView.Text = "Wyczyść";
            this.clearProductsDataGridView.UseVisualStyleBackColor = true;
            this.clearProductsDataGridView.Click += new System.EventHandler(this.clearProductsDataGridView_Click);
            // 
            // sumProductValueDataGridView
            // 
            this.sumProductValueDataGridView.AllowUserToAddRows = false;
            this.sumProductValueDataGridView.AllowUserToDeleteRows = false;
            this.sumProductValueDataGridView.AllowUserToResizeRows = false;
            this.sumProductValueDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sumProductValueDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.sumProductValueDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.sumProductValueDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sumProductValueDataGridView.Location = new System.Drawing.Point(15, 515);
            this.sumProductValueDataGridView.Name = "sumProductValueDataGridView";
            this.sumProductValueDataGridView.RowHeadersVisible = false;
            this.sumProductValueDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.sumProductValueDataGridView.Size = new System.Drawing.Size(759, 69);
            this.sumProductValueDataGridView.TabIndex = 19;
            // 
            // chosenProduktCntxt
            // 
            this.chosenProduktCntxt.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.chosenProduktCntxt.Name = "chosenProduktCntxt";
            this.chosenProduktCntxt.Size = new System.Drawing.Size(188, 48);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(187, 22);
            this.toolStripMenuItem1.Text = "Zmień ilość produktu";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 596);
            this.Controls.Add(this.sumProductValueDataGridView);
            this.Controls.Add(this.clearProductsDataGridView);
            this.Controls.Add(this.removeProductButton);
            this.Controls.Add(this.addProductButton);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.productAmount);
            this.Controls.Add(this.kcalTxtBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.carboTxtBox);
            this.Controls.Add(this.proteinTxtBox);
            this.Controls.Add(this.fatTxtBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.chosenProduktsDataGridView);
            this.Controls.Add(this.productsNameCmbBox);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Super Dieta";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chosenProduktsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sumProductValueDataGridView)).EndInit();
            this.chosenProduktCntxt.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox productsNameCmbBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem programToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zakończToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem produktyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dodajProduktToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem edytujProduktToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuńProduktToolStripMenuItem;
        private System.Windows.Forms.DataGridView chosenProduktsDataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox fatTxtBox;
        private System.Windows.Forms.TextBox proteinTxtBox;
        private System.Windows.Forms.TextBox carboTxtBox;
        private System.Windows.Forms.TextBox kcalTxtBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox productAmount;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button addProductButton;
        private System.Windows.Forms.Button removeProductButton;
        private System.Windows.Forms.Button clearProductsDataGridView;
        private System.Windows.Forms.DataGridView sumProductValueDataGridView;
        private System.Windows.Forms.ContextMenuStrip chosenProduktCntxt;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}

