﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Superdieta
{
    class ProductContainer
    {
        DataBase.DataBaseSupport dbSupport;
        Calculations oCalculation;

        ProductValue actualProduct;
        ProductValue computedProduct;

        // obiekt przechowujący wybrane produkty z obliczonymi wartościami odżywczymi
        DataTable chosenProductsTable;
        DataTable productsSumTable;

        public ProductContainer(DataBase.DataBaseSupport db) 
        {
            oCalculation = new Calculations();
            this.dbSupport = db;

            actualProduct = new ProductValue();
            computedProduct = new ProductValue();

            chosenProductsTable = new DataTable();
            productsSumTable = new DataTable();

            PrepareChosenProductsTable();
            PrepareProductsSumTable();
        }

   
        public void AddProduct(string amount)
        {
            string productName = computedProduct.productName;
            string productEnergy = computedProduct.kcalAmount;
            string productProtein = computedProduct.proteinAmount;
            string productFat = computedProduct.fatAmount;
            string productCarbo = computedProduct.carboAmount;
            string productAmountInGrams = amount;

            chosenProductsTable.Rows.Add(productName, productProtein, productFat, productCarbo, productEnergy, productAmountInGrams);
            productsSumTable = oCalculation.SumTwoProduct(computedProduct, productsSumTable);
        }

        // todo okodzić odejmowanie z tabeli sumy
        public void RemoveProduct(string name, string amount)
        {
            DataRow[] tmp = chosenProductsTable.Select("Nazwa = '" + name + "' and Ilość = '" + amount + "'");
            
            ProductValue tmpProduct = new ProductValue();
            tmpProduct.proteinAmount = tmp[0][1].ToString();
            tmpProduct.fatAmount = tmp[0][2].ToString();
            tmpProduct.carboAmount = tmp[0][3].ToString();
            tmpProduct.kcalAmount = tmp[0][4].ToString();

            productsSumTable = oCalculation.MinusTwoProduct(tmpProduct, productsSumTable);
            chosenProductsTable.Rows.Remove(tmp[0]);
        }

        public void UpdateProductAmount(string name, string amount, ProductValue product)
        {
            RemoveProduct(name, amount);

            product = oCalculation.GetProducIn100g(product, amount);
            product = oCalculation.CalculateProductValueInAmountCultureChange(product, product.productAmountInGrams);

            string productName = product.productName;
            string productEnergy = product.kcalAmount;
            string productProtein = product.proteinAmount;
            string productFat = product.fatAmount;
            string productCarbo = product.carboAmount;

            chosenProductsTable.Rows.Add(productName, productProtein, productFat, productCarbo, productEnergy, product.productAmountInGrams);            
            productsSumTable = oCalculation.SumTwoProduct(product, productsSumTable);
        }                                          

        public void ClearChosenProductTable()
        {
            this.chosenProductsTable.Rows.Clear();
            this.productsSumTable.Rows.Clear();
            productsSumTable.Rows.Add("0", "0", "0", "0", "0");
        }

        public void CalculateProductsValue(string amount)
        {
            this.computedProduct = oCalculation.CalculateProductValueInAmount(this.ActualProduct, amount);
        }


       public ProductValue GetProductFromDataBase(string productName)
        {
            ProductValue actual = new ProductValue();

            DataRow[] tmp = dbSupport.GetRecord(productName);

            actual.productName = tmp[0][1].ToString();
            actual.kcalAmount = tmp[0][2].ToString();
            actual.proteinAmount = tmp[0][3].ToString();
            actual.fatAmount = tmp[0][4].ToString();
            actual.carboAmount = tmp[0][5].ToString();

            return actual;
        }

       private void PrepareProductsSumTable()
       {
           productsSumTable.Columns.Add("Białko", typeof(string));
           productsSumTable.Columns.Add("Tłuszcze", typeof(string));
           productsSumTable.Columns.Add("Węglowodany", typeof(string));
           productsSumTable.Columns.Add("Energia [ kcal ]", typeof(string));
           productsSumTable.Columns.Add("Ilość", typeof(string));

           productsSumTable.Rows.Add( "0", "0", "0", "0", "0");
       }

        void PrepareChosenProductsTable()
        {
            chosenProductsTable.Columns.Add("Nazwa", typeof(string));
            chosenProductsTable.Columns.Add("Białko", typeof(string));
            chosenProductsTable.Columns.Add("Tłuszcze", typeof(string));
            chosenProductsTable.Columns.Add("Węglowodany", typeof(string));
            chosenProductsTable.Columns.Add("Energia [ kcal ]", typeof(string));
            chosenProductsTable.Columns.Add("Ilość", typeof(string));
        }


        public ProductValue ActualProduct
        {
            get { return actualProduct; }
            set { actualProduct = value; }
        }

        public ProductValue ComputedProduct
        {
            get { return computedProduct; }
            set { computedProduct = value; }
        }

        public DataTable ChosenProductTable
        {
            get { return chosenProductsTable; }
            set { chosenProductsTable = value; }
        }

        public DataTable SumProductValueTable
        {
            get { return productsSumTable; }
            set { productsSumTable = value; }
        }

    }
}
