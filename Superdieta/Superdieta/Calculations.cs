﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Data;

namespace Superdieta
{
    class Calculations
    {

        double ToDoble(string value)
        {
            double tmp = Convert.ToDouble(value);
            return tmp;
        }

        public ProductValue GetProducIn100g(ProductValue prValue, string amountInGrams)
        {
            ProductValue tmp = prValue;

            double protein = Convert.ToDouble(tmp.proteinAmount, CultureInfo.CurrentCulture);
            double fat = Convert.ToDouble(tmp.fatAmount, CultureInfo.CurrentCulture);
            double carbo = Convert.ToDouble(tmp.carboAmount, CultureInfo.CurrentCulture);
            double kcal = Convert.ToDouble(tmp.kcalAmount, CultureInfo.CurrentCulture);

            if (amountInGrams != null && amountInGrams != "")
            {
                double amount = Convert.ToDouble(amountInGrams) / 100;

                protein /= amount;
                fat /= amount;
                carbo /= amount;
                kcal /= amount;

                tmp.proteinAmount = protein.ToString();
                tmp.fatAmount = fat.ToString();
                tmp.carboAmount = carbo.ToString();
                tmp.kcalAmount = kcal.ToString();
            }
            else
            {
                tmp.proteinAmount = "0";
                tmp.fatAmount = "0";
                tmp.carboAmount = "0";
                tmp.kcalAmount = "0";
            }

            return tmp;
        }

        public ProductValue CalculateProductValueInAmountCultureChange(ProductValue prValue, string amountInGrams)
        {
            ProductValue tmp = prValue;

            double protein = Convert.ToDouble(tmp.proteinAmount, CultureInfo.CurrentCulture);
            double fat = Convert.ToDouble(tmp.fatAmount, CultureInfo.CurrentCulture);
            double carbo = Convert.ToDouble(tmp.carboAmount, CultureInfo.CurrentCulture);
            double kcal = Convert.ToDouble(tmp.kcalAmount, CultureInfo.CurrentCulture);

            if (amountInGrams != null && amountInGrams != "")
            {
                double amount = Convert.ToDouble(amountInGrams) / 100;

                protein *= amount;
                fat *= amount;
                carbo *= amount;
                kcal *= amount;

                tmp.proteinAmount = protein.ToString();
                tmp.fatAmount = fat.ToString();
                tmp.carboAmount = carbo.ToString();
                tmp.kcalAmount = kcal.ToString();
            }
            else
            {
                tmp.proteinAmount = "0";
                tmp.fatAmount = "0";
                tmp.carboAmount = "0";
                tmp.kcalAmount = "0";
            }

            return tmp;
        }

        public ProductValue CalculateProductValueInAmount(ProductValue prValue, string amountInGrams)
        {
            ProductValue tmp = prValue;

            double protein = Convert.ToDouble( tmp.proteinAmount, CultureInfo.InvariantCulture );
            double fat = Convert.ToDouble(tmp.fatAmount, CultureInfo.InvariantCulture);
            double carbo = Convert.ToDouble(tmp.carboAmount, CultureInfo.InvariantCulture);
            double kcal = Convert.ToDouble(tmp.kcalAmount, CultureInfo.InvariantCulture);

            if (amountInGrams != null && amountInGrams != "")
            {
                double amount = Convert.ToDouble(amountInGrams) / 100;

                protein *= amount;
                fat *= amount;
                carbo *= amount;
                kcal *= amount;

                tmp.proteinAmount = protein.ToString();
                tmp.fatAmount = fat.ToString();
                tmp.carboAmount = carbo.ToString();
                tmp.kcalAmount = kcal.ToString();
            }
            else
            {
                tmp.proteinAmount = "0";
                tmp.fatAmount     = "0";
                tmp.carboAmount   = "0";
                tmp.kcalAmount    = "0";
            }

            return tmp;
        }

        public DataTable SumTwoProduct(ProductValue productToAdd, DataTable sum)
        {
            DataTable tmp = new DataTable();

            double protein = Convert.ToDouble(productToAdd.proteinAmount, CultureInfo.CurrentCulture);
            double fat = Convert.ToDouble(productToAdd.fatAmount, CultureInfo.CurrentCulture);
            double carbo = Convert.ToDouble(productToAdd.carboAmount, CultureInfo.CurrentCulture);
            double kcal = Convert.ToDouble(productToAdd.kcalAmount, CultureInfo.CurrentCulture);

            protein += Convert.ToDouble( sum.Rows[0][0].ToString(), CultureInfo.CurrentCulture);
            fat += Convert.ToDouble( sum.Rows[0][1].ToString(), CultureInfo.CurrentCulture);
            carbo += Convert.ToDouble( sum.Rows[0][2].ToString(), CultureInfo.CurrentCulture);
            kcal += Convert.ToDouble( sum.Rows[0][3].ToString(), CultureInfo.CurrentCulture);   

            tmp = sum;
            tmp.Rows[0][0] = protein.ToString();
            tmp.Rows[0][1] = fat.ToString();
            tmp.Rows[0][2] = carbo.ToString();
            tmp.Rows[0][3] = kcal.ToString();

            return tmp;
        }


        public DataTable MinusTwoProduct(ProductValue productToMinus, DataTable sum)
        {
            DataTable tmp = new DataTable();

            double protein =   Convert.ToDouble(sum.Rows[0][0].ToString(), CultureInfo.CurrentCulture);
            double fat =       Convert.ToDouble(sum.Rows[0][1].ToString(), CultureInfo.CurrentCulture);
            double carbo =     Convert.ToDouble(sum.Rows[0][2].ToString(), CultureInfo.CurrentCulture);
            double kcal =      Convert.ToDouble(sum.Rows[0][3].ToString(), CultureInfo.CurrentCulture);

            protein -= Convert.ToDouble(productToMinus.proteinAmount, CultureInfo.CurrentCulture);
            fat -= Convert.ToDouble(productToMinus.fatAmount, CultureInfo.CurrentCulture);
            carbo -= Convert.ToDouble(productToMinus.carboAmount, CultureInfo.CurrentCulture);
            kcal -= Convert.ToDouble(productToMinus.kcalAmount, CultureInfo.CurrentCulture);    

            tmp = sum;
            tmp.Rows[0][0] = protein.ToString();
            tmp.Rows[0][1] = fat.ToString();
            tmp.Rows[0][2] = carbo.ToString();
            tmp.Rows[0][3] = kcal.ToString();

            return tmp;
        }
    }
}
