﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Threading;

namespace Superdieta
{
   // struktura przechowująca dane o produkcie
    public struct ProductValue
    {
       public string productName;
       public string proteinAmount;
       public string fatAmount;
       public string carboAmount;
       public string kcalAmount;
       public string productAmountInGrams;

    }
}