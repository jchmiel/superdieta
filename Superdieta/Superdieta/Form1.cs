﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Threading;

namespace Superdieta
{
    public partial class Form1 : Form
    {
        DataBase.DataBaseSupport dbSupport;
        Calculations oCalculations;
        ProductContainer oProductContainer;

        public delegate void ChangeProductAmountDelegate(string text);
        ChangeProductAmountDelegate deletgate;

        public Form1()
        {  
            InitializeComponent();

            deletgate += Metoda;

            dbSupport = new DataBase.DataBaseSupport();
            oCalculations = new Calculations();
            oProductContainer = new ProductContainer(this.dbSupport);

            OpenDataBase();
            PrepareWindow();          
        }

        public void Metoda(string text)
        {
            MessageBox.Show(text);
        }


        # region [ methods ]

        void PrepareWindow()
        {
            chosenProduktsDataGridView.ContextMenuStrip = chosenProduktCntxt;

            productAmount.Text = "100";
            kcalTxtBox.BackColor = Color.White;
            kcalTxtBox.ReadOnly = true;

            proteinTxtBox.BackColor = Color.White;
            proteinTxtBox.ReadOnly = true;

            fatTxtBox.BackColor = Color.White;
            fatTxtBox.ReadOnly = true;

            carboTxtBox.BackColor = Color.White;
            carboTxtBox.ReadOnly = true;
            FillProduktsCmbBox();

            oProductContainer.CalculateProductsValue(productAmount.Text);
            FillTextBoxes(oProductContainer.ActualProduct);

            chosenProduktsDataGridView.DataSource = oProductContainer.ChosenProductTable;
            sumProductValueDataGridView.DataSource = oProductContainer.SumProductValueTable;
        }


        void FillProduktsCmbBox()
        {
            DataTable tmp = null;
            tmp = dbSupport.GetProductsTable;

            if (tmp != null)
            {
                for (int i = 0; i < tmp.Rows.Count; i++)
                {
                    productsNameCmbBox.Items.Add(tmp.Rows[i][1].ToString());
                }

                productsNameCmbBox.SelectedIndex = 0;
            }

        }

        // metoda wyciągająca dane z rekordu
        void FillTextBoxes(ProductValue product)
        {
            proteinTxtBox.Text = product.proteinAmount;
            fatTxtBox.Text = product.fatAmount;
            carboTxtBox.Text = product.carboAmount;
            kcalTxtBox.Text = product.kcalAmount;
        }


        void OpenDataBase()
        {
            dbSupport.OpenDataBase();
        }

        void UpdateDataBase()
        {
            dbSupport.UpdateDataBase();
        }


        ProductValue GetProductToUpdate(string amount)
        {
            ProductValue product = new ProductValue();

            product.productName = chosenProduktsDataGridView.Rows[chosenProduktsDataGridView.SelectedRows[0].Index].Cells[0].Value.ToString();
            product.proteinAmount = chosenProduktsDataGridView.Rows[chosenProduktsDataGridView.SelectedRows[0].Index].Cells[1].Value.ToString();                      
            product.fatAmount = chosenProduktsDataGridView.Rows[chosenProduktsDataGridView.SelectedRows[0].Index].Cells[2].Value.ToString();
            product.carboAmount = chosenProduktsDataGridView.Rows[chosenProduktsDataGridView.SelectedRows[0].Index].Cells[3].Value.ToString();
            product.kcalAmount = chosenProduktsDataGridView.Rows[chosenProduktsDataGridView.SelectedRows[0].Index].Cells[4].Value.ToString();
            product.productAmountInGrams = amount;

            return product;
        }

        # endregion [ methods ]
   

        # region [ events ]

        private void zakończToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


        private void dodajProduktToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void edytujProduktToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void usuńProduktToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void chosenProduktsDataGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {   
            if (e.RowIndex != -1 && e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                chosenProduktsDataGridView.ClearSelection();                
            }
        }


        private void addProductButton_Click(object sender, EventArgs e)
        {
            oProductContainer.AddProduct(productAmount.Text);
        }

        private void removeProductButton_Click(object sender, EventArgs e)
        {
            if (chosenProduktsDataGridView.GetCellCount(DataGridViewElementStates.Selected) != 0)
            {

                int selectedRow = chosenProduktsDataGridView.SelectedRows[0].Index;
                oProductContainer.RemoveProduct(chosenProduktsDataGridView.Rows[selectedRow].Cells[0].Value.ToString(), chosenProduktsDataGridView.Rows[selectedRow].Cells[5].Value.ToString());
            }
        }

        
        private void clearProductsDataGridView_Click(object sender, EventArgs e)
        {
            oProductContainer.ClearChosenProductTable();
        }

        // todo wrzucić obliczenia do wyzerowania wartości
        private void productAmount_KeyUp(object sender, KeyEventArgs e)
        {
            ValidationClass.CheckTextBoxFillInteger(this.productAmount);
            oProductContainer.CalculateProductsValue(productAmount.Text);
            FillTextBoxes( oProductContainer.ComputedProduct);
        }


        private void productsNameCmbBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            oProductContainer.ActualProduct = oProductContainer.GetProductFromDataBase(productsNameCmbBox.SelectedItem.ToString());
            oProductContainer.CalculateProductsValue(productAmount.Text);
            FillTextBoxes(oProductContainer.ComputedProduct);
        }


         // todo zmiana ilości produktu
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            string amount = chosenProduktsDataGridView.Rows[chosenProduktsDataGridView.SelectedRows[0].Index].Cells[5].Value.ToString();
            string productName = chosenProduktsDataGridView.Rows[chosenProduktsDataGridView.SelectedRows[0].Index].Cells[0].Value.ToString();

            ChangeProduktAmountcs change = new ChangeProduktAmountcs(amount);
            change.ShowDialog();

            ProductValue product = GetProductToUpdate(change.ReturnAmount());
            oProductContainer.UpdateProductAmount(productName, amount, product);
        }

        # endregion [ events ]           
       
    }
}
