﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Threading;

namespace Superdieta
{
    class ValidationClass
    {
        // metoda sprawdzająca, czy w textBoxach są tylko liczby   
        public static string ReturnOnlyIntegerNumber(string text)
        {
            string wzorzec = @"^(\d)*$";
            Regex test = new Regex(wzorzec);

            for (int i = 0; i < text.Length; i++)
            {
                if (!test.IsMatch(string.Concat(text[i])))
                {
                    text = text.Remove(i, 1);
                }
            }

            return text;
        }

        public static bool IsIntegerNumber(string text)
        {
            bool isOnlyDigit;
            string wzorzec = @"^(\d)*$";
            Regex test = new Regex(wzorzec);

            if (!test.IsMatch(text))
            {
                isOnlyDigit = false;
            }
            else
            {
                isOnlyDigit = true;
            }

            return isOnlyDigit;
        }

        /// <summary>
        /// Methods return textBox with only integer number 
        /// </summary>
        /// <param name="txtBox"></param>
        public static void CheckTextBoxFillInteger(TextBox txtBox)
        {
            txtBox.Text = ValidationClass.ReturnOnlyIntegerNumber(txtBox.Text);

            if (ValidationClass.IsIntegerNumber(txtBox.Text) == true)
            {
                Thread.Sleep(50);
            }

            if (txtBox.Text.Length >= 1)
            {
                txtBox.SelectionStart = txtBox.Text.Length;
            }
        }


    }
}
